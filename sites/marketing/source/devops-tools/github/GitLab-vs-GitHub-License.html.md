---
layout: markdown_page
title: "GitLab vs GitHub License Comparison"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab Core vs GitHub Free

**License Gaps**

![GitLab GitHub Free Gaps Chart](/devops-tools/github/images/GitHub-Free-Gaps.png)

**Questions and Risk When Considering GitHub Free**

![GitLab GitHub Free Questions Chart](/devops-tools/github/images/GitHub-Free-Questions.png)

## GitLab Starter/Bronze vs GitHub Team

**License Gaps**

![GitLab GitHub Team Gaps Chart](/devops-tools/github/images/GitHub-Team-Gaps.png)

**Questions and Risk When Considering GitHub Team**

![GitLab GitHub Team Questions Chart](/devops-tools/github/images/GitHub-Team-Question.png)

## GitLab Premium/Silver vs GitHub Enterprise

**License Gaps**

![GitLab GitHub Enterprise Gaps Chart](/devops-tools/github/images/GitHub-Enterprise-Gaps.png)

**Questions and Risk When Considering Enterprise Team**

![GitLab GitHub Enterprise Quetions Chart](/devops-tools/github/images/GitHub-Enterprise-Questions.png)
