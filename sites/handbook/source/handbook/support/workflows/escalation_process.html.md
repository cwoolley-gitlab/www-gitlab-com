---
layout: markdown_page
title: Support <> TAM Escalations
category: Handling tickets
---

### When to escalate a ticket to a Technical Account Manager (TAM)

- If a customer submits either an Emergency or High Priority ticket and there is a TAM associated with the account, copy the TAM onto the ticket and @ mention them on Slack with the ticket details.
- If you have a long running ticket, or, if you believe the relationship with customer is at risk due to expectations from the ticket, copy the TAM onto the ticket and @ mention them on Slack with ticket details. It is also beneficial to @ mention support-managers as an FYI.

Details on the TAM Escalation process can be found here:  
[Customer Success Escalation Process](/handbook/customer-success/tam/escalations/index.html)
