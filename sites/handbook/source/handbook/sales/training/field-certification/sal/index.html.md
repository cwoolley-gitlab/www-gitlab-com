---
layout: handbook-page-toc
title: "Field Certification: Strategic Account Leaders"
---

# Field Certification Program for Strategic Account Leaders
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a  certification program for Strategic Account Leaders that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the customer journey and critical “Moments That Matter” (MTMs) as well as the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute each MTM.

# Strategic Account Leader Curriculum 
The below slide shows the holsitic learner journey for SALs, and provides context for the information included throughout the learning program. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1mhzoJMJSyx4wz47g-0P2dUQJc2hTHflZUhE57mcN83g/edit#slide=id.g94bb3b04a3_0_271" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

# Pipeline Generation & Qualifaction Learning Path 
This is the first learning path in development for SALs. The course will consist of 5 courses, the learning objectives for each course are below.

**Course 1: Applying DevOps**
* Apply DevOps Concepts and Terms 
* Explain the different types of use cases 
* Given a scenario, determine an entry point with a customer 

**Course 2: Describe the Customer Journey** 
* Describe sales activities aligned to the customer journey (OODA LOOP)
* Define a Marketing Qualified Lead 
* Convert the Marketing Qualified Lead into a Sales Accepted Opportunity 
* Differentiate between territory planning, opportunity planning, and account planning 

**Course 3: Analyze Your Territory**
* Analyze a territory 
* Given a scenario, select a pipeline engagement activities 
* Given a business vertical, create and present a territory plan

**Course 4: Execute Pipeline Generation Activities** 
* Differentiate between lead generation and pipeline generation
* Describe how to conduct various pipelines generation activities 
* Prepare for an initial qualification meeting 
* Conduct an initial qualification meeting (OODA)

**Course 5: Manage Forecasting Activities**
* Differentiate between pipelines management and forecasting
* Describe pipelines best practices
* Calculate your quote gap
* Determine your ideal pipeline size to meet your quota 
* Explain the process to accurately forecast 

# Recognition
Upon completing each course, the associate will receieve a badge. 

# Feedback 
We want to hear from you! You can follow along with course development by checking out the issues on the [Field Cert Team Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426?&label_name[]=field%20certification). 


